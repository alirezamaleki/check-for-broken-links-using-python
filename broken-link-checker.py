# import pandas, requests, and datetime modules
import pandas as pd
import requests
import datetime

# current date
current_date = datetime.datetime.today()

# import data from links .csv file using Pandas (in the same working directory)
df = pd.read_csv('links.csv')

# disable chained assignments
pd.options.mode.chained_assignment = None 

for i, row in df.iterrows():
    try:
        url = df.loc[i,'URL']
        request = requests.get(url)
        if request.status_code == 200:
            print('good' + ', ' + url)
            df.loc[i, 'Link_Status'] = 'good'
            df.loc[i, 'Check_Date'] = current_date
        else:
            print('bad' + ', ' + url)
            df.loc[i, 'Link_Status'] = 'bad'
            df.loc[i, 'Check_Date'] = current_date
    except:
        pass

# add the results to 'results.csv' file (in the same working directory)
df.to_csv('results.csv')