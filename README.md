# Check for broken links using Python

The purpose of this code is to get a list of links (web urls) from a .csv file and validate the links. It uses "Requests" python module to validate the links. If the link validation fails, the link status will be equal to "bad", otherwise "good"